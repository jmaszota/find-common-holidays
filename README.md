## Demo

Directory _demo_ contains packaged, ready to run JAR. To play with application only JRE is needed.
To run the app open command prompt, navigate to demo directory  and execute: _java -jar find-common-holidays-0.0.1-SNAPSHOT.jar_ 

## Getting Started

### Prerequisites

* JDK 8+
* Maven 3.2+

### Running the app

It is SpringBoot application. To start it, simply run the mvn command
  
  _mvn spring-boot:run_ 

Application will be available under the url [http://localhost:8080/swagger-ui.html]

_To improve performance in-memory cache is enabled. Data from external provider is being cached. The TTL can be controlled by application property *cache.ttl*. Default value is 60 sec_

### Configuration

Application can be configured using standard SpringBoot configuration files (application.properties)

Used config parameters:
* ext.holidayprovider.calendarific.url=
* ext.holidayprovider.calendarific.apikey=
* logging.level.com.maszota=DEBUG
* cache.ttl=
 
### Reference

* Running SpringBoot: [https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-running-your-application.html]
* Holidays provider: [https://calendarific.com]
* Supported countries: [https://calendarific.com/supported-countries]


