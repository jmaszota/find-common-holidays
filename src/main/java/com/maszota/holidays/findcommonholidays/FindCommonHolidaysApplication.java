package com.maszota.holidays.findcommonholidays;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FindCommonHolidaysApplication {

  public static void main(String[] args) {
    SpringApplication.run(FindCommonHolidaysApplication.class, args);
  }

}
