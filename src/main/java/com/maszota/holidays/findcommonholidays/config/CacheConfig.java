package com.maszota.holidays.findcommonholidays.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Slf4j
@Configuration
@EnableCaching
@EnableScheduling
public class CacheConfig {

  public static final String HOLIDAYS_CACHE = "HOLIDAYS_CACHE";

  @CacheEvict(allEntries = true, cacheNames = {HOLIDAYS_CACHE})
  @Scheduled(fixedDelayString = "${cache.ttl}")
  public void cacheEvict() {
    log.trace("Deleting cache: " + HOLIDAYS_CACHE);
  }

}
