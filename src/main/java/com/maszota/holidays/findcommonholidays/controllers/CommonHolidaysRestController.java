package com.maszota.holidays.findcommonholidays.controllers;

import com.maszota.holidays.findcommonholidays.dtos.CommonHolidays;
import com.maszota.holidays.findcommonholidays.exceptions.ClientException;
import com.maszota.holidays.findcommonholidays.services.CommonHolidaysService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "Endpoint for common holidays")
@RestController
@RequestMapping("/api/common-holidays")
public class CommonHolidaysRestController {

  private final CommonHolidaysService commonHolidaysService;

  public CommonHolidaysRestController(CommonHolidaysService commonHolidaysService) {
    this.commonHolidaysService = commonHolidaysService;
  }


  @ApiOperation(value = "Returns first common holiday", notes = "Returns first common holiday (time wise) that occurs after specified date.", response = CommonHolidays.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successful retrieval of common holiday if found", response = CommonHolidays.class),
      @ApiResponse(code = 204, message = "No common holiday found"),
      @ApiResponse(code = 404, message = "When one of parameters is not given or is invalid"),
      @ApiResponse(code = 500, message = "Internal server error")}
  )
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CommonHolidays> getCommonHolidays(
      @ApiParam(value = "Date after which the common holiday should be found. Format: YYYY-MM-DD", required = true)
      @RequestParam("date")
      @DateTimeFormat(iso = ISO.DATE)
          LocalDate fromDate,
      @ApiParam(value = "First country to compare", required = true)
      @RequestParam("countryA")
          String countryA,
      @ApiParam(value = "Second country to compare", required = true)
      @RequestParam("countryB")
          String countryB) throws ClientException {

    CommonHolidays commonHolidayAfter = commonHolidaysService.getCommonHolidayAfter(fromDate, countryA, countryB);
    if (commonHolidayAfter == null) {
      return ResponseEntity.noContent().build();
    }
    return ResponseEntity.ok(commonHolidayAfter);

  }

}
