package com.maszota.holidays.findcommonholidays.controllers;

import com.maszota.holidays.findcommonholidays.dtos.ApiError;
import com.maszota.holidays.findcommonholidays.exceptions.ClientException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

  @Override
  protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    String error = "Internal Server Error";
    return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, error, ex));
  }


  @ExceptionHandler({ClientException.class})
  protected ResponseEntity<Object> handleClientException(ClientException ex) {
    String error = "Exception occurred when handling data provider request";
    ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR);
    apiError.setMessage(error);
    apiError.setDebugMessage(ex.getMessage());
    return buildResponseEntity(apiError);
  }


  @ExceptionHandler({IllegalArgumentException.class})
  protected ResponseEntity<Object> handleIllegalArgument(IllegalArgumentException ex) {
    String error = "Exception occurred when handling data provider request";
    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
    apiError.setMessage(error);
    apiError.setDebugMessage(ex.getMessage());
    return buildResponseEntity(apiError);
  }

  private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
    return new ResponseEntity<>(apiError, apiError.getStatus());
  }

  //other exception handlers below

}
