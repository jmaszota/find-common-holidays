package com.maszota.holidays.findcommonholidays.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@ApiModel
public class CommonHolidays {

  @ApiModelProperty(position = 1, required = true, value = "Date of the common holiday")
  private LocalDate date;
  @ApiModelProperty(required = true, value = "Name of the holiday in 1st country")
  private String name1;
  @ApiModelProperty(required = true, value = "Name of the holiday in 2nd country")
  private String name2;

}
