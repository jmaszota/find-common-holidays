package com.maszota.holidays.findcommonholidays.dtos;

import java.time.LocalDate;
import java.util.Objects;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Holiday {

  private LocalDate date;
  private String name;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Holiday holiday = (Holiday) o;
    return date.equals(holiday.date) &&
        name.equals(holiday.name);
  }

  public boolean equalsOnDate(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Holiday holiday = (Holiday) o;
    return date.equals(holiday.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, name);
  }
}
