package com.maszota.holidays.findcommonholidays.ext.providers;

import com.maszota.holidays.findcommonholidays.dtos.Holiday;
import com.maszota.holidays.findcommonholidays.exceptions.ClientException;
import java.util.List;

public interface HolidaysProvider {

  List<Holiday> provideHolidays(int year, String countryCode) throws ClientException;

}
