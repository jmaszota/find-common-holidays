package com.maszota.holidays.findcommonholidays.ext.providers.calendraific.dtos;

import lombok.Data;

@Data
public class CalendarificDate {

  private DateTime datetime;

}
