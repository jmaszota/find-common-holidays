package com.maszota.holidays.findcommonholidays.ext.providers.calendraific.dtos;

import lombok.Data;

@Data
public class CalendarificHoliday {

  private String name;
  private String description;
  private CalendarificDate date;
  private String[] type;
  private String locations;
  private String states;

}
