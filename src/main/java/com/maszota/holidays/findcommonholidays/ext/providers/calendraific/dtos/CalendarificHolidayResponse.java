package com.maszota.holidays.findcommonholidays.ext.providers.calendraific.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;
import lombok.Data;

@Data
public class CalendarificHolidayResponse {

  private MetaData meta;

  @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
  private List<HolidayResponse> response;


}
