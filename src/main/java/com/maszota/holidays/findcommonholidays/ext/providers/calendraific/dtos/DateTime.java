package com.maszota.holidays.findcommonholidays.ext.providers.calendraific.dtos;

import lombok.Data;

@Data
public class DateTime {

  private int year;
  private int month;
  private int day;

}
