package com.maszota.holidays.findcommonholidays.ext.providers.calendraific.dtos;

import java.util.List;
import lombok.Data;

@Data
public class HolidayResponse {

  private List<CalendarificHoliday> holidays;

}
