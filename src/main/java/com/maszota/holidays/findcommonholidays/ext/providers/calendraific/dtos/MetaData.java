package com.maszota.holidays.findcommonholidays.ext.providers.calendraific.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MetaData {

  private int code;
  @JsonProperty("error_type")
  private String errorType;
  @JsonProperty("error_detail")
  private String errorDetail;
}
