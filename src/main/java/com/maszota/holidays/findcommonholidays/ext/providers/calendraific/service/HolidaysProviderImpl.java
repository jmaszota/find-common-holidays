package com.maszota.holidays.findcommonholidays.ext.providers.calendraific.service;

import static com.maszota.holidays.findcommonholidays.config.CacheConfig.HOLIDAYS_CACHE;

import com.google.common.base.Strings;
import com.maszota.holidays.findcommonholidays.dtos.Holiday;
import com.maszota.holidays.findcommonholidays.exceptions.ClientException;
import com.maszota.holidays.findcommonholidays.ext.providers.HolidaysProvider;
import com.maszota.holidays.findcommonholidays.ext.providers.calendraific.dtos.CalendarificHolidayResponse;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
@Setter
public class HolidaysProviderImpl implements HolidaysProvider {


  @Value("${ext.holidayprovider.calendarific.url}")
  private String apiUrl;

  @Value("${ext.holidayprovider.calendarific.apikey}")
  private String apiKey;

  private String urlTemplate = "%s?country=%s&year=%d&api_key=%s";

  private final RestTemplate restTemplate;

  public HolidaysProviderImpl() {

    CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
    requestFactory.setHttpClient(httpClient);

    restTemplate = new RestTemplate(requestFactory);
  }

  @Cacheable(HOLIDAYS_CACHE)
  @Override
  public List<Holiday> provideHolidays(int year, String countryCode) throws ClientException {

    String tempUrl = String.format(urlTemplate, apiUrl, countryCode, year, apiKey);

    ResponseEntity<CalendarificHolidayResponse> getHolidays;

    try {
      log.debug("Calling {}", tempUrl);

      getHolidays = restTemplate.getForEntity(tempUrl, CalendarificHolidayResponse.class);

    } catch (RestClientException rce) {
      log.warn("Exception occurred calling client", rce);
      throw new ClientException("Caught exception when calling data provider", rce);
    }

    if (!getHolidays.getStatusCode().equals(HttpStatus.OK)) {
      log.warn("Data provider return status {}", getHolidays.getStatusCode());
      throw new ClientException("Data provider return status " + getHolidays.getStatusCode());
    }

    if (getHolidays.getBody().getResponse().size() == 0) {
      if (!Strings.isNullOrEmpty(getHolidays.getBody().getMeta().getErrorType())) {
        log.warn("Data provider returned error_type: {}, with error_detail: {}", getHolidays.getBody().getMeta().getErrorType(),
            getHolidays.getBody().getMeta().getErrorDetail());
        //throw new IllegalArgumentException("Unknown country code: " + countryCode);
      }
      return new ArrayList<>();
    }

    List<Holiday> commonHolidays = getHolidays.getBody().getResponse().get(0).getHolidays().stream().map(
        h -> Holiday.builder().name(h.getName()).date(
            LocalDate.of(h.getDate().getDatetime().getYear(), h.getDate().getDatetime().getMonth(), h.getDate().getDatetime().getDay()))
            .build()
    ).collect(Collectors.toList());

    return commonHolidays;
  }
}
