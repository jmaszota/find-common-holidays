package com.maszota.holidays.findcommonholidays.services;

import com.maszota.holidays.findcommonholidays.dtos.CommonHolidays;
import com.maszota.holidays.findcommonholidays.exceptions.ClientException;
import java.time.LocalDate;

public interface CommonHolidaysService {

  CommonHolidays getCommonHolidayAfter(LocalDate dateFrom, String countryA, String countryB) throws ClientException;

}
