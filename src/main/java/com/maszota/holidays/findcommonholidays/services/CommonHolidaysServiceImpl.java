package com.maszota.holidays.findcommonholidays.services;

import com.google.common.base.Strings;
import com.maszota.holidays.findcommonholidays.dtos.CommonHolidays;
import com.maszota.holidays.findcommonholidays.dtos.Holiday;
import com.maszota.holidays.findcommonholidays.exceptions.ClientException;
import com.maszota.holidays.findcommonholidays.ext.providers.HolidaysProvider;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class CommonHolidaysServiceImpl implements CommonHolidaysService {

  private final HolidaysProvider holidaysProvider;

  public CommonHolidaysServiceImpl(HolidaysProvider holidaysProvider) {
    this.holidaysProvider = holidaysProvider;
  }

  @Override
  public CommonHolidays getCommonHolidayAfter(LocalDate dateFrom, String countryA, String countryB) throws ClientException {

    if (dateFrom == null || Strings.isNullOrEmpty(countryA) || Strings.isNullOrEmpty(countryB)) {
      throw new IllegalArgumentException("All arguments must be provided");
    }

    List<Holiday> holidaysCountryA = holidaysProvider.provideHolidays(dateFrom.getYear(), countryA);
    List<Holiday> holidaysCountryB = holidaysProvider.provideHolidays(dateFrom.getYear(), countryB);

    return findFirstCommonHoliday(dateFrom, holidaysCountryA, holidaysCountryB);
  }

  public static CommonHolidays findFirstCommonHoliday(LocalDate afterDate, List<Holiday> holidaysA, List<Holiday> holidaysB) {

    if (holidaysA == null || holidaysA.isEmpty() || holidaysB == null || holidaysB.isEmpty()) {
      return null;
    }

    List<Holiday> preparedHolidaysA = holidaysA.stream().filter(f -> f.getDate().isAfter(afterDate))
        .sorted(Comparator.comparing(Holiday::getDate))
        .collect(Collectors.toList());

    List<Holiday> preparedHolidaysB = holidaysB.stream().filter(f -> f.getDate().isAfter(afterDate))
        .sorted(Comparator.comparing(Holiday::getDate))
        .collect(Collectors.toList());

    for (Holiday holidayA : preparedHolidaysA) {
      for (Holiday holidayB : preparedHolidaysB) {
        if (holidayA.equalsOnDate(holidayB)) {
          return CommonHolidays.builder().date(holidayA.getDate()).name1(holidayA.getName()).name2(holidayB.getName()).build();
        }
      }
    }

    return null;
  }
}
