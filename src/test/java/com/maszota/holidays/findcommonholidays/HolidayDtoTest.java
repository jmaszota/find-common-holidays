package com.maszota.holidays.findcommonholidays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.maszota.holidays.findcommonholidays.dtos.Holiday;
import java.time.LocalDate;
import org.junit.Test;

public class HolidayDtoTest {

  @Test
  public void equals(){
    Holiday holiday1 = Holiday.builder().date(LocalDate.now()).name("Easter").build();
    Holiday holiday2 = Holiday.builder().date(LocalDate.now()).name("Easter").build();

    assertTrue(holiday1.equals(holiday2));

    holiday2 = Holiday.builder().date(LocalDate.now().plusDays(1)).name("Easter").build();

    assertFalse(holiday1.equals(holiday2));

    holiday2 = Holiday.builder().date(LocalDate.now()).name("Easter1").build();

    assertFalse(holiday1.equals(holiday2));

    holiday2 = Holiday.builder().date(LocalDate.now()).build();

    assertFalse(holiday1.equals(holiday2));
  }

}
