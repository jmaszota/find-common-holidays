package com.maszota.holidays.findcommonholidays.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.maszota.holidays.findcommonholidays.dtos.CommonHolidays;
import com.maszota.holidays.findcommonholidays.exceptions.ClientException;
import com.maszota.holidays.findcommonholidays.services.CommonHolidaysService;
import java.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(CommonHolidaysRestController.class)
public class CommonHolidaysRestControllerTests {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private CommonHolidaysService commonHolidaysService;

  @Test
  public void getCommonHolidays_missingQueryParam() throws Exception {

    mockMvc.perform(get("/api/common-holidays")).andExpect(status().isBadRequest());
    mockMvc.perform(get("/api/common-holidays?date=2019-04-01")).andExpect(status().isBadRequest());
    mockMvc.perform(get("/api/common-holidays?countryA=NO")).andExpect(status().isBadRequest());

  }

  @Test
  public void getCommonHolidays_badDataFormat() throws Exception {

    mockMvc.perform(get("/api/common-holidays?date=20190401&countryA=NO&countryB=PL")).andExpect(status().isBadRequest());

  }

  @Test
  public void getCommonHolidays_okRequest() throws Exception {

    when(commonHolidaysService.getCommonHolidayAfter(eq(LocalDate.of(2019, 4, 1)), eq("NO"), eq("PL")))
        .thenReturn(CommonHolidays.builder().name1("NO_name").name2("PL_name").date(LocalDate.of(2019, 05, 01)).build());

    mockMvc.perform(get("/api/common-holidays?date=2019-04-01&countryA=NO&countryB=PL"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(content().json("{'name1': 'NO_name','name2': 'PL_name','date': '2019-05-01'}"));


  }

  @Test
  public void getCommonHolidays_204WhenNoContent() throws Exception {

    mockMvc.perform(get("/api/common-holidays?date=2019-04-01&countryA=NO&countryB=PL")).andExpect(status().isNoContent());

  }


  @Test
  public void getCommonHolidays_500WhenClienException() throws Exception {

    doThrow(ClientException.class).when(commonHolidaysService).getCommonHolidayAfter(any(), any(), any());

    mockMvc.perform(get("/api/common-holidays?date=2019-04-01&countryA=NO&countryB=PL")).andExpect(status().isInternalServerError());

  }
}
