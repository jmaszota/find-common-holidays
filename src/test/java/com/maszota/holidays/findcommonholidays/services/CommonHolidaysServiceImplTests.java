package com.maszota.holidays.findcommonholidays.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import com.maszota.holidays.findcommonholidays.ext.providers.HolidaysProvider;
import com.maszota.holidays.findcommonholidays.dtos.CommonHolidays;
import com.maszota.holidays.findcommonholidays.dtos.Holiday;
import com.maszota.holidays.findcommonholidays.exceptions.ClientException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CommonHolidaysServiceImplTests {

  private CommonHolidaysServiceImpl commonHolidaysService;

  @Mock
  private HolidaysProvider holidaysProvider;

  @Before
  public void setUp() {

    MockitoAnnotations.initMocks(this);

    commonHolidaysService = new CommonHolidaysServiceImpl(holidaysProvider);
  }

  @Test
  public void getCommonHolidayAfter_missingArgument() {
    assertThrows(IllegalArgumentException.class, () -> commonHolidaysService.getCommonHolidayAfter(null, "PL", "NO"));
    assertThrows(IllegalArgumentException.class, () -> commonHolidaysService.getCommonHolidayAfter(null, null, null));
    assertThrows(IllegalArgumentException.class, () -> commonHolidaysService.getCommonHolidayAfter(LocalDate.now(), null, null));
    assertThrows(IllegalArgumentException.class, () -> commonHolidaysService.getCommonHolidayAfter(LocalDate.now(), "PL", null));
    assertThrows(IllegalArgumentException.class, () -> commonHolidaysService.getCommonHolidayAfter(LocalDate.now(), null, "NO"));
  }

  @Test
  public void getCommonHolidayAfter_unknownCountry() throws ClientException {
    when(holidaysProvider.provideHolidays(eq(2019),eq("PL"))).thenReturn(Stream.of(Holiday.builder().date(LocalDate.of(2019, 5, 9)).name("HolidayPL_2").build()).collect(
        Collectors.toList()));

    when(holidaysProvider.provideHolidays(eq(2019),eq("DUP"))).thenReturn(new ArrayList<>());

    assertNull(commonHolidaysService.getCommonHolidayAfter(LocalDate.of(2019, 04, 01), "PL", "DUP"));
  }

  @Test
  public void getCommonHolidayAfter_externalException() throws ClientException {

    doThrow(new ClientException("SomeError")).when(holidaysProvider).provideHolidays(eq(2019), eq("PL"));

    assertThrows(ClientException.class,
        () -> commonHolidaysService.getCommonHolidayAfter(LocalDate.of(2019, 04, 01), "PL", "NO"));
  }

  @Test
  public void getCommonHolidayAfter_nothingInCommon() throws ClientException {
    List<Holiday> holidaysA = Stream.of(Holiday.builder().date(LocalDate.of(2019, 6, 9)).name("HolidayPL_3").build(),
        Holiday.builder().date(LocalDate.of(2019, 5, 9)).name("HolidayPL_2").build(),
        Holiday.builder().date(LocalDate.of(2019, 4, 9)).name("HolidayPL_1").build()).collect(
        Collectors.toList());

    List<Holiday> holidaysB = Stream.of(Holiday.builder().date(LocalDate.of(2019, 7, 9)).name("HolidayNO_2").build(),
        Holiday.builder().date(LocalDate.of(2019, 8, 9)).name("HolidayNO_1").build(),
        Holiday.builder().date(LocalDate.of(2019, 9, 9)).name("HolidayNO_3").build()).collect(
        Collectors.toList());

    when(holidaysProvider.provideHolidays(eq(2019), eq("HolidayPL"))).thenReturn(holidaysA);
    when(holidaysProvider.provideHolidays(eq(2019), eq("HolidayNO"))).thenReturn(holidaysB);

    CommonHolidays firstCommonHoliday = commonHolidaysService.getCommonHolidayAfter(LocalDate.of(2019, 04, 01), "HolidayPL", "HolidayNO");
    assertNull(firstCommonHoliday);
  }

  @Test
  public void getCommonHolidayAfter_onlyOneFound() throws ClientException {

    when(holidaysProvider.provideHolidays(eq(2019), eq("HolidayPL"))).thenReturn(defaultListOfHolidays("HolidayPL"));
    when(holidaysProvider.provideHolidays(eq(2019), eq("HolidayNO"))).thenReturn(defaultListOfHolidays("HolidayNO"));

    CommonHolidays firstCommonHoliday = commonHolidaysService.getCommonHolidayAfter(LocalDate.of(2019, 04, 01), "HolidayPL", "HolidayNO");
    assertEquals(LocalDate.of(2019, 4, 9), firstCommonHoliday.getDate());
    assertEquals("HolidayPL_1", firstCommonHoliday.getName1());
    assertEquals("HolidayNO_1", firstCommonHoliday.getName2());

  }

  @Test
  public void findFirstCommonHoliday_sameDatesSorted() {

    List<Holiday> holidaysA = defaultListOfHolidays("HolidayA");

    List<Holiday> holidaysB = defaultListOfHolidays("HolidayB");

    CommonHolidays firstCommonHoliday = CommonHolidaysServiceImpl.findFirstCommonHoliday(LocalDate.of(2019, 04, 01), holidaysA, holidaysB);
    assertEquals(LocalDate.of(2019, 4, 9), firstCommonHoliday.getDate());
    assertEquals("HolidayA_1", firstCommonHoliday.getName1());
    assertEquals("HolidayB_1", firstCommonHoliday.getName2());
  }

  @Test
  public void findFirstCommonHoliday_sameDatesUnSorted() {

    List<Holiday> holidaysA = Stream.of(Holiday.builder().date(LocalDate.of(2019, 6, 9)).name("HolidayA_3").build(),
        Holiday.builder().date(LocalDate.of(2019, 5, 9)).name("HolidayA_2").build(),
        Holiday.builder().date(LocalDate.of(2019, 4, 9)).name("HolidayA_1").build()).collect(
        Collectors.toList());

    List<Holiday> holidaysB = Stream.of(Holiday.builder().date(LocalDate.of(2019, 5, 9)).name("HolidayB_2").build(),
        Holiday.builder().date(LocalDate.of(2019, 4, 9)).name("HolidayB_1").build(),
        Holiday.builder().date(LocalDate.of(2019, 6, 9)).name("HolidayB_3").build()).collect(
        Collectors.toList());

    CommonHolidays firstCommonHoliday = CommonHolidaysServiceImpl.findFirstCommonHoliday(LocalDate.of(2019, 04, 01), holidaysA, holidaysB);
    assertEquals(LocalDate.of(2019, 4, 9), firstCommonHoliday.getDate());
    assertEquals("HolidayA_1", firstCommonHoliday.getName1());
    assertEquals("HolidayB_1", firstCommonHoliday.getName2());
  }

  @Test
  public void findFirstCommonHoliday_Asymmetric() {

    List<Holiday> holidaysA = Stream.of(Holiday.builder().date(LocalDate.of(2019, 6, 9)).name("HolidayA_3").build(),
        Holiday.builder().date(LocalDate.of(2019, 5, 9)).name("HolidayA_2").build(),
        Holiday.builder().date(LocalDate.of(2019, 4, 9)).name("HolidayA_1").build()).collect(
        Collectors.toList());

    List<Holiday> holidaysB = Stream.of(Holiday.builder().date(LocalDate.of(2019, 5, 9)).name("HolidayB_2").build(),
        Holiday.builder().date(LocalDate.of(2019, 6, 9)).name("HolidayB_3").build()).collect(
        Collectors.toList());

    CommonHolidays firstCommonHoliday = CommonHolidaysServiceImpl.findFirstCommonHoliday(LocalDate.of(2019, 04, 01), holidaysA, holidaysB);
    assertEquals(LocalDate.of(2019, 5, 9), firstCommonHoliday.getDate());
    assertEquals("HolidayA_2", firstCommonHoliday.getName1());
    assertEquals("HolidayB_2", firstCommonHoliday.getName2());
  }

  @Test
  public void findFirstCommonHoliday_holidayAEmpty() {

    List<Holiday> holidaysA = new ArrayList<>();

    List<Holiday> holidaysB = Stream.of(Holiday.builder().date(LocalDate.of(2019, 5, 9)).name("HolidayB_2").build()).collect(
        Collectors.toList());

    CommonHolidays firstCommonHoliday = CommonHolidaysServiceImpl.findFirstCommonHoliday(LocalDate.of(2019, 04, 01), holidaysA, holidaysB);
    assertNull(firstCommonHoliday);
  }

  @Test
  public void findFirstCommonHoliday_differentDates() {

    List<Holiday> holidaysA = Stream.of(Holiday.builder().date(LocalDate.of(2019, 6, 9)).name("HolidayA_3").build(),
        Holiday.builder().date(LocalDate.of(2019, 5, 9)).name("HolidayA_2").build(),
        Holiday.builder().date(LocalDate.of(2019, 4, 9)).name("HolidayA_1").build()).collect(
        Collectors.toList());

    List<Holiday> holidaysB = Stream.of(Holiday.builder().date(LocalDate.of(2019, 7, 9)).name("HolidayB_2").build(),
        Holiday.builder().date(LocalDate.of(2019, 8, 9)).name("HolidayB_1").build(),
        Holiday.builder().date(LocalDate.of(2019, 9, 9)).name("HolidayB_3").build()).collect(
        Collectors.toList());

    CommonHolidays firstCommonHoliday = CommonHolidaysServiceImpl.findFirstCommonHoliday(LocalDate.of(2019, 04, 01), holidaysA, holidaysB);
    assertNull(firstCommonHoliday);
  }

  @Test
  public void findFirstCommonHoliday_holidayBEmpty() {

    List<Holiday> holidaysB = new ArrayList<>();

    List<Holiday> holidaysA = Stream.of(Holiday.builder().date(LocalDate.of(2019, 5, 9)).name("HolidayB_2").build()).collect(
        Collectors.toList());

    CommonHolidays firstCommonHoliday = CommonHolidaysServiceImpl.findFirstCommonHoliday(LocalDate.of(2019, 04, 01), holidaysA, holidaysB);
    assertNull(firstCommonHoliday);
  }

  private List<Holiday> defaultListOfHolidays(final String holidayNamePrefix) {
    return Stream.of(Holiday.builder().date(LocalDate.of(2019, 4, 9)).name(holidayNamePrefix + "_1").build(),
        Holiday.builder().date(LocalDate.of(2019, 5, 9)).name(holidayNamePrefix + "_2").build(),
        Holiday.builder().date(LocalDate.of(2019, 6, 9)).name(holidayNamePrefix + "_3").build()).collect(
        Collectors.toList());
  }
}
